// @ts-ignore
import ICAL from 'ical.js';
import * as converter from '../utils/dateConversions'

export class CourseModel {
    name: string;
    location: string;
    group: string;
    lastModification: string;
    start: Date;
    end: Date;
    uid: string;
    courseNumber;

    constructor(ev: Array<any>) {
        const stringStart = ev[1][1][3];
        const stringEnd = ev[1][2][3];
        this.name = ev[1][3][3];
        this.location = ev[1][4][3];
        if (this.location === '') {
            this.location = '(pas de salle définie)';
        }
        const description = ev[1][5][3].replaceAll('\n', ' ');
        const whereModification = description.lastIndexOf('(Modif');
        this.lastModification = description.substring(whereModification)
            .replace('(', '')
            .replace(')', '');
        this.group = description.substring(0, whereModification).trim();
        this.start = new Date(stringStart);
        this.end = new Date(stringEnd);
        this.uid = ev[1][6][3];
        this.courseNumber = parseInt(this.name.substr(2, 3));
    }

    getDate() {
        return converter.DateToString(this.start);
    }

}

interface ResultWithStatus<T> {
    result: T,
    status: string,
    error: boolean
}

async function fetchUrl(url: string): Promise<ResultWithStatus<string>> {
    return fetch('https://sautax.alwaysdata.net/p/api/proxy', {
        body: url,
        method: 'POST',
    })
        .then(value => value.text())
        .then((result) => {
            return {result, status: "", error: false}
        })
        .catch(() => {
            return {result: "", status: "error when fetching", error: true}
        })
}

function bakeUrl(originalUrl: string) {
    let today = new Date(Date.now());
    let firstDate = new Date(today);
    firstDate.setDate(firstDate.getDate() - 30);
    let lastDate = new Date(today);
    lastDate.setDate(lastDate.getDate() + 70);
    let url = '';
    if (originalUrl.length > 0) {
        url = originalUrl.substring(0, originalUrl.lastIndexOf('&firstDate='));
        url += '&firstDate=' + converter.DateToAdeString(firstDate) + '&lastDate=' + converter.DateToAdeString(lastDate);
        return url;
    }
    return null;

}


function parse(content: string): ResultWithStatus<Map<string, Array<CourseModel>>> {
    let result = new Map() as Map<string, Array<CourseModel>>
    try {

        let icalParsed = ICAL.parse(content)

        if (icalParsed && icalParsed.length && icalParsed.length > 2 && icalParsed[2]) {// check parsing

            // index by day
            for (const ev of icalParsed[2]) {
                let cours = new CourseModel(ev);
                let day = cours.getDate();
                let currentArray = result.get(day);

                if (!currentArray)
                    currentArray = [];
                currentArray.push(cours);
                result.set(day, currentArray);
            }

            // sort by hour
            result.forEach((day_components) => {

                if (day_components) {
                    day_components = day_components.sort((a, b) => {
                        return a.start.getTime() - b.start.getTime();
                    });
                }
            });
        } else  // parse error
            return {result, status: "parsingError", error: true}


    } catch (e) {
        return {result, status: "parsingError", error: true}

    }
    return {result, status: "", error: false}

}

export async function getCourses(url: string, fetchFirst = false): Promise<ResultWithStatus<Map<string, Array<CourseModel>>>> {

    let result = new Map() as Map<string, Array<CourseModel>>;
    let bakedUrl = bakeUrl(url);
    let status = ""
    const today = new Date(Date.now());

    const last_fetch_date_str = localStorage.getItem('last_fetch_date');
    const last_fetch_content = localStorage.getItem('last_fetch_content');
    const last_url = localStorage.getItem('last_url');
    let cacheLooksValid = false;
    let fetchNeeded = fetchFirst;

    let content = "";

    if (last_fetch_date_str && last_fetch_date_str.length > 0 &&
        last_fetch_content && last_fetch_content.length > 0 &&
        last_url === url && last_url !== '') { // if cache looks valid
        cacheLooksValid = true;
        const last_fetch_date = new Date(last_fetch_date_str)
        if (today.getTime() - last_fetch_date.getTime() > 1000 * 60 * 60 * 12) // if cache is less than 12h old
            fetchNeeded = true;

    } else fetchNeeded = true;

    if (bakedUrl === null) {
        return {result, status: "invalidUrl", error: true};
    }

    let fetchError;

    // if we need to fetch
    if (fetchNeeded) {
        let fetchRes = await fetchUrl(bakedUrl)
        if (fetchRes.error) {
            fetchError = true;
        } else {
            // fetch succeeded
            content = fetchRes.result;
            status = "fetch"
        }
    }
    // if we can’t get it
    if (!fetchNeeded || fetchError) { // load cache
        if (!cacheLooksValid || !last_fetch_content || last_fetch_content.length < 1) { // we can’t reach the cache and we tell the compiler that last_fetch_content is a string
            return {result, status: "noCache+fetchError", error: true}
        } else {
            content = last_fetch_content;
            status = "cache"
        }
    }
    let parseRes = parse(content)
    // if the parse failed
    if (parseRes.error) {
        if (fetchError) // if we checked the the cache and fetched
            return {result, status: "noCache+fetchError", error: true}
        // try to fetch
        let fetchRes = await fetchUrl(bakedUrl)
        status = "fetch"
        if (fetchRes.error)
            return {result, status: "noCache+fetchError", error: true}
        else {
            // fetch succeeded
            content = fetchRes.result;
            parseRes = parse(content)
            if (parseRes.error) { // if we failed to parse
                return {result, status: "noCache+fetchError", error: true}
            }
        }
    }
    result = parseRes.result;
    if (status === 'fetch') {
        localStorage.setItem('last_url', url);
        localStorage.setItem('last_fetch_date', today.toString());
        localStorage.setItem('last_fetch_content', content)
    }

    return {result, status, error: false};
}

/*
export class CalendarModel {

    courseArray: Array<CourseModel> = [];
    status: string = "";
    currentUrl: string;

    constructor(url: string) {
        this.currentUrl = url
        this.init();
    }


    init() {
        this.courseArray = [];
        status = "";
        let today = new Date(Date.now());
        let firstDate = new Date(today)
        firstDate.setDate(firstDate.getDate() - 30)
        let lastDate = new Date(today)
        lastDate.setDate(lastDate.getDate() + 70)
        let url = '';
        if (this.currentUrl.length > 0) {
            url = this.currentUrl.substring(0, this.currentUrl.lastIndexOf('&firstDate='))
            url += '&firstDate=' + DateToAdeString(firstDate) + '&lastDate=' + DateToAdeString(lastDate)

        }

    }


}*/

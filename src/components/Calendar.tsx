import React from 'react';
import { Course } from './Course';
import * as converter from '../utils/dateConversions';
import { CourseModel } from "../model/CalendarModel";
import { getIndexNextCourse } from "../utils/nextCourse";
import { DateToString } from "../utils/dateConversions";

interface CaledarProps {
    result: Map<string, Array<CourseModel>>,
    error: boolean,
    emptyUrl: boolean,
    cached: boolean,
    loading: boolean,
}

interface CalendarState {
    chosen_day: string,
}


export default class Calendar extends React.Component<CaledarProps, CalendarState> {
    constructor(props: CaledarProps) {
        super(props);
        let today = new Date(Date.now());
        this.state = {
            chosen_day: converter.DateToString(today),
        };
    }


    offsetDay(n: number) {
        let d = converter.StringToDate(this.state.chosen_day)
        d.setDate(d.getDate() + n)
        this.setState({ chosen_day: converter.DateToString(d) })
    }

    today() {
        const today = new Date(Date.now());
        const str = converter.DateToString(today)
        this.setState({ chosen_day: str })
    }

    render() {
        if (this.props.loading) {
            return (<div>Chargement du calendrier</div>)
        }
        if (this.props.emptyUrl) {
            return (<div>Vous devez définir l’adresse de votre calendrier dans les paramètres</div>);
        }

        const todayStr = DateToString(new Date(Date.now()));
        let res = this.props.result.get(this.state.chosen_day);
        let out = [];

        if (res && res.length > 0) {
            let currentCourseIndex = getIndexNextCourse(res);
            for (const index in res) {
                out.push(<Course key={res[index].uid} input={res[index]}
                    current={index === currentCourseIndex && todayStr === this.state.chosen_day} />);
            }
        } else {
            out.push(<p key={'no-course'}>vous n’avez pas cours !</p>);
        }

        const chosen_day_array = this.state.chosen_day.split('/');
        const printed_day = new Date(parseInt(chosen_day_array[2]), parseInt(chosen_day_array[1]) - 1, parseInt(chosen_day_array[0]));
        let loadStatus = ''
        if (this.props.cached) {
            let cacheDate = localStorage.getItem("last_fetch_date")
            let date = ''
            if (cacheDate) {
                date = ' du ' + new Date(cacheDate).toLocaleString('fr', {
                    day: 'numeric',
                    month: 'numeric',
                    hour: '2-digit',
                    minute: '2-digit'
                })

            }
            loadStatus = 'Affichage d’une version en cache' + date;
        } else if (!this.props.error) {
            loadStatus = 'Nouvelle version chargée'
        }
        /*
                if (this.state.loading)
                    return (<div>Chargement du calendrier...</div>);
        */
        if (this.props.error)
            return (<div>erreur lors du chargement du calendrier</div>);

        return (<div id={'cal-view'}>
            <div id={'load-status'}>{loadStatus}</div>
            <button id={'button-prev'} className={'button'} onClick={() => this.offsetDay(-1)}>Précédent</button>
            <button id={'button-today'} className={'button'} onClick={() => this.today()}>Aujourd’hui</button>
            <button id={'button-next'} className={'button'} onClick={() => this.offsetDay(+1)}>Suivant</button>
            <div className={"day-view current-day"}>

                <p id={'day-date'}>
                    {printed_day.toLocaleDateString('fr', { day: 'numeric', month: 'short', weekday: 'short' })}
                </p>
                {out}
            </div>
        </div>);
    }
}
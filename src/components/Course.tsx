import React from 'react';
import { CourseModel } from "../model/CalendarModel";


// some dark material colors :

const defaultColor = "#37474F"

const colors = [
    "#C62828",
    "#C51162",
    "#7B1FA2",
    "#4A148C",
    "#5E35B1",
    "#311B92",
    "#303F9F",
    "#1A237E",
    "#0277BD",
    "#01579B",
    "#00796B",
    "#004D40",
    "#1B5E20",
    "#BF360C",
    "#3E2723",
    "#212121",
    "#455A64",
    "#757575",
    "#263238",
]


interface CourseProps {
    input: CourseModel,
    current: boolean,
}

interface CourseState {
}

export class Course extends React.Component<CourseProps, CourseState> {

    render() {

        const dateOptions: Intl.DateTimeFormatOptions = {
            hour12: false,
            minute: '2-digit',
            hour: '2-digit',
        };
        const courseNumber = parseInt(this.props.input.name.substr(2, 3));

        let color = defaultColor;
        if (!isNaN(courseNumber) && courseNumber >= 0) {
            let divided = courseNumber / 10;
            let big = Math.floor(divided);
            let little = (divided - big) * 10;
            let n = big + little;
            n = Math.round(n) - 10;
            color = colors[n % colors.length];
        } else { // if the name is not in the expected format
            let sum = 0;
            let end = this.props.input.name.length;
            if (end > 10) {
                end = end - 5; // don’t get the last 5 chars if the name is long, as it usually contains the group 
            }
            // compute the sum
            for (let i = 0; i < end; ++i) {
                sum += this.props.input.name.charCodeAt(i);
            }
            color = colors[sum % colors.length];
        }
        const courseStyle = {
            backgroundColor: color,
        }
        let currentClass = 'course';
        if (this.props.current) {
            currentClass += ' current-course';
        }

        return (
            <div className={currentClass} key={this.props.input.uid}
                style={courseStyle}>
                <div className={'course-start'}>{this.props.input.start.toLocaleString('fr-FR', dateOptions)} </div>
                <div className={'course-pipe'}>|</div>
                <div className={'course-end'}>{this.props.input.end.toLocaleString('fr-FR', dateOptions)}</div>
                <div className={'course-name'}>{this.props.input.name}</div>
                <div className={'course-group'}>{this.props.input.group}</div>
                <div className={'course-location'}>{this.props.input.location}</div>
            </div>
        );
    }
}
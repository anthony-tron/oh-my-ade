import React from "react";

interface SettingsProps {
    url: string,
    onChange: (event: React.ChangeEvent<HTMLInputElement>) => void,
    forceRefresh: () => void,
}

export class Settings extends React.Component<SettingsProps, any> {

    render() {
        return (
            <div id={'view'}>
                <p className={'view-name'}>settings</p>
                <label>
                    <div className={'url-label'}>Url du calendrier en ical (<a
                        href={'https://gitlab.com/nilsponsard/oh-my-ade/-/wikis/Comment-obtenir-l%E2%80%99url-du-calendrier'}>comment
                        l’obtenir</a>) :
                    </div>
                    <input className={'input-text'} type={'text'}
                        onChange={(event) => this.props.onChange(event)}
                        value={this.props.url} /></label>
            </div>
        )
    }
}
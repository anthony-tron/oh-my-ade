import {CourseModel} from "../model/CalendarModel";

export function getIndexNextCourse(courses: Array<CourseModel>) {
    let currentTime = new Date(Date.now())
    let min = 24 * 60 * 60 * 1000 // 24h
    let minIndex = null
    for (const index in courses) {
        let diff = courses[index].end.getTime() - currentTime.getTime()
        if (diff > 0 && diff < min) {
            min = diff;
            minIndex = index
        }
    }
    return minIndex;
}
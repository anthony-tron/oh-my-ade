import React from 'react';
import 'materialize-css';
import 'materialize-css/dist/css/materialize.min.css';
import './App.css';
import Calendar from './components/Calendar';
import { Settings } from './components/Settings';
import { CourseModel, getCourses } from "./model/CalendarModel";
import {Tab, Tabs} from "react-materialize";


interface AppState {
    url: string,
    result: Map<string, Array<CourseModel>>,
    error: boolean,
    cached: boolean,
    loading: boolean,
}


class App extends React.Component<any, AppState> {

    constructor(props: any) {
        super(props);
        let url = localStorage.getItem('last_url')
        if (!url) {
            url = ''
        }

        this.state = { url, result: new Map<string, Array<CourseModel>>(), error: false, cached: false, loading: true };
    }

    async componentDidMount() {
        await this.populate();
    }


    async populate(forceRefresh = false) {
        console.log("fetching : " + this.state.url)
        let output = await getCourses(this.state.url, forceRefresh);
        let result = output.result
        let error = output.error
        let cached = output.status === 'cache'
        this.setState({ result, error, cached, loading: false })
    }

    onChangeHandler(event: React.ChangeEvent<HTMLInputElement>) {
        let url = event.target.value

        this.setState({ url, loading: true, error: false, cached: false }, () => {
            this.populate(true);
        })

    }


    render() {
        return (
            <div className="App">
                <div className="App-content">
                    <div id={'content'}>
                        <Tabs>
                            <Tab title="Mes cours">
                                <Calendar
                                    error={this.state.error}
                                    result={this.state.result}
                                    emptyUrl={this.state.url === ''}
                                    cached={this.state.cached}
                                    loading={this.state.loading} />
                            </Tab>

                            <Tab title="Paramètres">
                                <Settings
                                    url={this.state.url}
                                    onChange={(event) => this.onChangeHandler(event)}
                                    forceRefresh={() => this.populate(true)} />
                            </Tab>
                        </Tabs>
                    </div>

                    <div className={'footer'}>
                        Développé par Nils Ponsard, <a
                        href={'https://gitlab.com/nilsponsard/oh-my-ade'}>code source</a>
                    </div>
                </div>
            </div>
        );
    }
}


export default App;
